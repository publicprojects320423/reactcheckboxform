# ReactCheckboxForm



## Description:

- The form starts with one checkbox, and if the last checkbox is checked, a new unchecked checkbox is added.
- There must always be at least one checkbox checked.
- Each checked checkbox displays a segment in a chevron chart, which occupies the entire width of the available space.
- On a mobile device, the chevron chart is rotated by -90 degrees and occupies the entire available height, with all segments - being the same size.
- The color of the segment is determined by the order of the checked checkbox.

