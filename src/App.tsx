import React, { useState } from "react";
import ChevronChart from "./components/ChevronChart";
import CheckboxForm from "./components/CheckboxForm";

const App = () => {
    const [checkboxes, setCheckboxes] = useState<boolean[]>([true, false]);

    const handleCheckboxChange = (index: number) => {
        setCheckboxes((prevCheckboxes) => {
            const newCheckboxes = [...prevCheckboxes];
            newCheckboxes[index] = !newCheckboxes[index];

            if (index === newCheckboxes.length - 1 && newCheckboxes[index]) {
                newCheckboxes.push(false);
            }

            if (newCheckboxes.every((checked) => !checked)) {
                return prevCheckboxes;
            }

            return newCheckboxes;
        });
    };

    return (
        <div className="app">
            <CheckboxForm
                checkboxes={checkboxes}
                onChange={handleCheckboxChange}
            />
            <ChevronChart checkboxes={checkboxes} />
        </div>
    );
};

export default App;
