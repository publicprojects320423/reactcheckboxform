import React from "react";

interface ChevronChartProps {
    checkboxes: boolean[];
}

const ChevronChart: React.FC<ChevronChartProps> = ({ checkboxes }) => {
    const checkedBoxes = checkboxes.filter((checked) => checked);

    const getColor = (index: number, total: number) => {
        const h = 190;
        const s = `${Math.floor(33 - (index / total) * 32)}%`;
        const l = `${Math.floor(18 + (index / total) * 80)}%`;

        return `hsl(${h}, ${s}, ${l})`;
    };

    const colors = checkedBoxes.map((_, index) =>
        getColor(index, checkedBoxes.length)
    );

    return (
        <div className="chevron-chart">
            <ul>
                {colors.map((color, index) => (
                    <li
                        key={index}
                        className="chevron-segment"
                        style={{
                            backgroundColor: color,
                            width: `${100 / colors.length}%`,
                        }}
                    >
                        <div
                            className="chevron-segment-before"
                            style={{
                                borderRight: `20px solid ${color}`,
                            }}
                        />
                        <div
                            className="chevron-segment-after"
                            style={{
                                borderRight: `20px solid ${color}`,
                            }}
                        />
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default ChevronChart;
