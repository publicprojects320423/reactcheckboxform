import React from "react";

interface CheckboxFormProps {
    checkboxes: boolean[];
    onChange: (index: number) => void;
}

const CheckboxForm: React.FC<CheckboxFormProps> = ({
    checkboxes,
    onChange,
}) => {
    return (
        <form>
            {checkboxes.map((checked, index) => (
                <label key={index}>
                    <input
                        type="checkbox"
                        checked={checked}
                        onChange={() => onChange(index)}
                    />
                    Checkbox {index + 1}
                </label>
            ))}
        </form>
    );
};

export default CheckboxForm;
